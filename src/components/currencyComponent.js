//Методы, относящиеся к странице обмена валют

/**
 * Переход в раздел "Валюта"
 * @param page - текущий экземпляр страницы
 */
exports.goToCurrencyPage = async (page) => {
    await page.click('#externaltraderoom-index');
};

/**
 * Заполнение поля суммы
 * @param page - текущий экземпляр страницы
 * @param quantity - сумма
 */
exports.setCurrencyQuantity = async (page, quantity) => {
    await page.fill('input.credit', quantity);
};

/**
 * переход во вкладку "Биржевой мост"
 * @param page - текущий экземпляр страницы
 */
exports.switchToCurrencyBridge = async (page) => {
    await page.click('#bankRateExtendedTab');
};

/**
 * Переход во вкладку "Мой курс"
 * @param page - текущий экземпляр страницы
 */
exports.switchToMyCurrencyBridge = async (page) => {
    await page.click('#myRateTab');
};