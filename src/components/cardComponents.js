//методы, относящиеся к странице карт
const utils = require('../utils')

/**
 * Переход в раздел "Карты"
 * @param page - текущий экземпляр страницы
 */
exports.goToCardPage = async (page) => {
    await page.click('#cards-overview-index');
};

/**
 * Добавление новой карты
 * @param page - текущий экземпляр страницы
 * @param pathToCard - xPath путь к карте
 */
exports.addNewCard = async (page, pathToCard) => {
    await utils.click(page,'a#order-new-card-link');
    await page.waitForSelector('select#type-select');
    await page.selectOption('select#type-select', {index: 8});
    await utils.click(page, pathToCard);
    await page.waitForSelector('select#card-branch[size=\'1\']');
    await page.selectOption('select#card-branch[size=\'1\']', {index: 2});
    await page.waitForSelector('//button[text()=\'Заказать\']');
};

/**
 * Добавление новой карты
 * @param page - текущий экземпляр страницы
 * @param pathToCard - xPath путь к карте
 */
exports.addCreditCard = async (page, pathToCard) => {
    await utils.click(page,'a#order-new-card-link');
    await page.waitForSelector('select#type-select');
    await page.selectOption('select#type-select', {index: 8});
    await utils.click(page, pathToCard);
    await page.fill('[data-min]', '200000');
    await page.click('[name=\'condition.creditHistory\']');
    await page.click('[name=\'condition.personalDataProcessing\']');
    await page.click('[name=\'condition.mobileSubscriberDataProcessing\']');
    await page.waitForSelector('[data-ga-action]');
};
