//Обертки над методами выполнения Api запроса
const axios = require('axios');

/**
 * Настройки для axios
 */
const setAxiosDefault =  () => {
    axios.defaults.baseURL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/';
    axios.defaults.headers.common['Authorization'] = `Token ${process.env.DADATA_TOKEN}`;
};

/**
 * Обертка над методом get
 * @param url - Конец ссылки на сервис
 * @return результат запроса
 */
exports.get = async (url) =>{
    setAxiosDefault();
    return await axios.get(url);
};

/**
 * Обертка над методом post
 * @param url - Конец ссылки на сервис
 * @param query - Тело запроса
 * @return Результат запроса
 */
exports.post = async (url, query) =>{
    setAxiosDefault()
    return await axios.post(url, query);
};

/**
 * Валидирование JsonSchema
 * @param ajv - валидатор
 * @param schema - схема валидации
 * @param parameter - данные, которые надо валидировать
 * @return Результат валидации
 */
exports.validateJsonSchema = async (ajv, schema, parameter) =>{
    return await ajv.validate(schema, parameter);
};