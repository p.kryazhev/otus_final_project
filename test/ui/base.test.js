const { chromium } = require('playwright');
const authorizationComponent = require('../../src/components/authorizationComponent');
const utils = require('../../src/utils');
const statementComponent = require('../../src/components/statementComponent');
const paymentComponent = require('../../src/components/paymentComponent');
const cardComponent = require('../../src/components/cardComponents');
const depositComponent = require('../../src/components/depositComponents');
const userComponent = require('../../src/components/userComponent');
const currencyComponent = require('../../src/components/currencyComponent');

describe('online bank test', () => {

    let page;
    let browser;

    beforeEach(async () => {
        browser = await chromium.launch({headless: true, args: ['--no-sandbox']});
        page = await browser.newPage();
        await authorizationComponent.authorization(page);
    });

    afterEach(async () =>{
        const screenshotBuffer = await page.screenshot();
        await reporter.addAttachment("Screenshot", screenshotBuffer, "image/png");
        await browser.close();
    });

    test('payment test', async () => {
        await paymentComponent.goToMobilePayment(page);
        await paymentComponent.setMobilePaymentFields(page);
        expect('50.00 ₽').toEqual(await utils.getText(page, 'span#fee-amount'));
    });

    /*test('statement test', async () => {
        await statementComponent.goToStatement(page);
        await statementComponent.setStatementFilter(page);
        expect(`Период: ${utils.getCurrentDate()} - ${utils.getCurrentDate()}`).toEqual(await utils.getText(page, '.statement-header > div:nth-child(3)'));
    });*/

    test('add travel premium card test', async () => {
        await cardComponent.goToCardPage(page);
        await cardComponent.addNewCard(page, '[data-debit-card-event=\'formSubmissionSuccess_travelp\']');
        expect('Заказать').toEqual(await utils.getText(page, '//button[text()=\'Заказать\']'));
    });
    test('add salary card test', async () => {
        await cardComponent.goToCardPage(page);
        await cardComponent.addNewCard(page, '[data-debit-card-event="formSubmissionSuccess_wages"]');
        expect('Заказать').toEqual(await utils.getText(page, '//button[text()=\'Заказать\']'));
    });

    test('add cashback card test', async () => {
        await cardComponent.goToCardPage(page);
        await cardComponent.addCreditCard(page, '[data-debit-card-event="formSubmissionSuccess_cashback"]');
        expect('Отправить заявку').toEqual(await utils.getText(page, '//button[text()=\'Отправить заявку\']'));
    });
    test('add pension card test', async () => {
        await cardComponent.goToCardPage(page);
        await cardComponent.addCreditCard(page, '[data-debit-card-event="formSubmissionSuccess_mirpension"]');
        expect('Отправить заявку').toEqual(await utils.getText(page, '//button[text()=\'Отправить заявку\']'));
    });

    test('deposit info test', async () => {
        await depositComponent.goToDeposit(page);
        expect(utils.getCurrentDate()).toEqual(await utils.getText(page, '#deposit-orderDate > div > span'));
    });

    test('order cash test', async () => {
        await userComponent.switchToULUser(page);
        await statementComponent.goToCashbox(page);
        await statementComponent.addNewCashWithdrawal(page);
        expect('Подтверждение заявки на бронирование ').toEqual(await utils.getText(page, '.page-header > h1'));
    });

    test('currency exchange test', async () => {
        await currencyComponent.goToCurrencyPage(page);
        await page.waitForLoadState();
        const handle = await page.$('iframe.full-page');
        const contentFrame = await handle.contentFrame();
        await currencyComponent.setCurrencyQuantity(contentFrame, '200');
        expect(await utils.isVisible(contentFrame, 'div.confirm')).toEqual(true);
    });

    test('currency bridge test', async () => {
        await currencyComponent.goToCurrencyPage(page);
        await page.waitForSelector('iframe.full-page');
        const handle = await page.$('iframe.full-page');
        const contentFrame = await handle.contentFrame();
        await currencyComponent.switchToCurrencyBridge(contentFrame);
        expect(await utils.isVisible(contentFrame, '.currency-pair-container.clearfix.ng-scope')).toEqual(true);
    });

    test('my currency bridge test', async () => {
        await currencyComponent.goToCurrencyPage(page);
        await page.waitForLoadState();
        const handle = await page.$('iframe.full-page');
        const contentFrame = await handle.contentFrame();
        await currencyComponent.switchToMyCurrencyBridge(contentFrame);
        expect(await utils.isVisible(contentFrame, '.currency-pair-container.clearfix.ng-scope')).toEqual(true);
    });
});
