let assert = require('assert');
let Ajv= require('ajv');
const apiProvider = require('../../src/lib/apiProvider');

describe('dadata api tests', () =>{
    const ajv = new Ajv({$data: true, logger: console, allErrors: true, verbose: true});
    let schema;

    test('dadata check ip location test', async () => {
        schema = {"properties" : {
                "value" : {'type' : 'string'},
                "unrestricted_value" : {'type' : 'string'}
            },
            "required": ["value", "unrestricted_value"]};
        const r = await apiProvider.get('/iplocate/address?ip=46.226.227.20');
        const a = await apiProvider.validateJsonSchema(ajv, schema, r.data.location);
        expect(r.status).toBe(200);
        assert.deepEqual(a, true);
    });

    test('dadata find post station by number', async () => {
        const query = { "postal_unit": "127642" }
        const r = await apiProvider.post('geolocate/postal_unit', query);
        expect(r.status).toBe(200);
    });

    test('dadata find post station by coordinates', async () => {
        const query = {
            "query": "Молодогвардейская",
            "filters": [
                {
                    "address_kladr_id": "6300000100000"
                }
            ]
        }
        const r = await apiProvider.post('geolocate/postal_unit', query);
        expect(r.status).toBe(200);
    });

    test('dadata find post station with filter', async () => {
        const query = { "lat": 55.878, "lon": 37.653, "radius_meters": 1000 }
        const r = await apiProvider.post('geolocate/postal_unit', query);
        expect(r.status).toBe(200);
    });

    test('dadata find post station exclude closed', async () => {
        const query = {
            "query": "105",
            "filters": [
                {
                    "is_closed": false
                }
            ]
        }
        const r = await apiProvider.post('geolocate/postal_unit', query);
        expect(r.status).toBe(200);
    });

    test('dadata get info by inn', async () => {
        schema = {"properties" : {
                "suggestions" : {'type' : 'array'}
            },
            "required": ["suggestions"]};
        const query = {query: '9705100963'};
        const r = await apiProvider.post('/suggest/party', query);
        const a = await apiProvider.validateJsonSchema(ajv, schema, r.data);
        expect(r.status).toBe(200);
        assert.deepEqual(a, true);
    });

    test('dadata get car info', async () => {
        schema = {"properties" : {
                "suggestions" : {'type' : 'array'}
            },
            "required": ["suggestions"]};
        const query = {query: 'FORD'};
        const r = await apiProvider.post('findById/car_brand', query);
        const a = await apiProvider.validateJsonSchema(ajv, schema, r.data);
        expect(r.status).toBe(200);
        assert.deepEqual(a, true);
    });

    test('dadata get car info - rus', async () => {
        schema = {"properties" : {
                "suggestions" : {'type' : 'array'}
            },
            "required": ["suggestions"]};
        const query = {query: 'Форд'};
        const r = await apiProvider.post('findById/car_brand', query);
        const a = await apiProvider.validateJsonSchema(ajv, schema, r.data);
        expect(r.status).toBe(200);
        assert.deepEqual(a, true);
    });

    test('dadata get metro station info 1', async () => {
        schema = {"properties" : {
                "suggestions" : {'type' : 'array'}
            },
            "required": ["suggestions"]};
        const query = {query: 'Бабушкинская'};
        const r = await apiProvider.post('suggest/metro', query);
        const a = await apiProvider.validateJsonSchema(ajv, schema, r.data);
        expect(r.status).toBe(200);
        assert.deepEqual(a, true);
    });

    test('dadata get metro station info 2', async () => {
        schema = {"properties" : {
                "suggestions" : {'type' : 'array'}
            },
            "required": ["suggestions"]};
        const query = {
            "query": "па",
            "filters": [
                {
                    "city": "Москва",
                    "line_id": "5",
                    "is_closed": false
                }
            ]
        };
        const r = await apiProvider.post('suggest/metro', query);
        const a = await apiProvider.validateJsonSchema(ajv, schema, r.data);
        expect(r.status).toBe(200);
        assert.deepEqual(a, true);
    });

    test('dadata get metro station info 3', async () => {
        schema = {"properties" : {
                "suggestions" : {'type' : 'array'}
            },
            "required": ["suggestions"]};
        const query = {
            "query": "алек",
            "filters": [
                {
                    "city_kladr_id": "7800000000000"
                }
            ]
        };
        const r = await apiProvider.post('suggest/metro', query);
        const a = await apiProvider.validateJsonSchema(ajv, schema, r.data);
        expect(r.status).toBe(200);
        assert.deepEqual(a, true);
    });
});