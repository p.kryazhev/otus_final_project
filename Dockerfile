FROM mcr.microsoft.com/playwright:bionic
RUN apt-get update && \
    rm -rf /var/lib/apt/lists/*
ADD ./package.json /app/package.json
WORKDIR /app
RUN npm i
ADD ./ /app
